-- [SECTION] Add new records

-- Add artists
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Fearless",
    "2008-01-01",
    3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Red",
    "2012-01-01",
    3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "A Star Is Born",
    "2018-01-01",
    4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Born This Way",
    "2011-01-01",
    4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Purpose",
    "2015-01-01",
    5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
    "Dangerous Woman",
    "2016-01-01",
    6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Fearless",
    246,
    "Pop rock",
    3
);
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "State of Grace",
    253,
    "Rock, alternative rock, arena rock",
    4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Black Eyes",
    151,
    "Rock",
    5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Born This Way",
    252,
    "Electropop",
    6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Sorry",
    212,
    "Dancehall",
    7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
    "Into You",
    242,
    "EDM House",
    8
);

-- [SECTION] Advanced selects

-- Exclude records
SELECT * FROM songs WHERE id != 5;

-- Greater than (or equal to)
SELECT * FROM songs WHERE id >= 4;

-- Less than (or equal to)
SELECT * FROM songs WHERE id < 7;

-- Get specific IDs (OR)
-- We can specify multiple columns using OR
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 6;

-- Get specific IDs (IN)
-- We only specify only one column using IN
SELECT * FROM songs WHERE id IN (1, 5, 6) OR genre = "OPM";

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%e"; -- select keyword from the end.
SELECT * FROM songs WHERE song_name LIKE "b%"; -- select keyword from the start.
SELECT * FROM songs WHERE song_name LIKE "%a%"; -- select keyword in between.

-- Sort records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Limit returned records
SELECT * FROM songs ORDER BY song_name DESC LIMIT 3;

-- Offset/skip records
SELECT * FROM songs LIMIT 5 OFFSET 2;

-- Getting distinct records
SELECT DISTINCT genre FROM songs;

-- Count
SELECT COUNT(*) from songs WHERE genre LIKE "%rock%";

-- [SECTION] Table joins

-- Combine artists and albums table (INNER JOIN)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- Left join (show artists regardless of album status)
SELECT * FROM artists LEFT JOIN albums ON artists.id = albums.artist_id;

-- Right join (show artists regardless of album status)
SELECT * FROM albums RIGHT JOIN artists ON albums.artist_id = artists.id;

SELECT * FROM artists 
    LEFT JOIN albums on artists.id = albums.artist_id
    LEFT JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists 
    LEFT JOIN albums on artists.id = albums.artist_id
    LEFT JOIN songs ON albums.id = songs.album_id;
