USE music_db;

SELECT * FROM artists WHERE name LIKE "%d%";

SELECT * FROM songs WHERE length > 230;

SELECT albums.album_title, songs.song_name, songs.length FROM albums
    LEFT JOIN songs on songs.album_id = albums.id;

SELECT * FROM albums
    LEFT JOIN artists ON artists.id = albums.artist_id
    WHERE albums.album_title LIKE "%a%";

SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

SELECT * FROM albums 
    LEFT JOIN songs ON songs.album_id = albums.id
    ORDER BY albums.album_title DESC, songs.song_name;
    